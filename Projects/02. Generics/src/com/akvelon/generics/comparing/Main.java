package com.akvelon.generics.comparing;

import java.util.Arrays;
import java.util.Scanner;

import static com.akvelon.generics.comparing.ComparingUtil.sort;

/**
 * 06.04.2022
 * 02. Generics
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {


    public static void main(String[] args) {
        User[] users = {
                new User(10, "Марсель", "Сидиков", 28),
                new User(15, "Айрат", "Мухутдинов", 23),
                new User(2, "Виктор", "Евлампьев", 24),
                new User(1, "Ринат", "Сафин", 24),
                new User(6, "Алия", "Сидиков", 21),
                new User(12, "Зульфат", "Мухутдинов", 29)
        };

        Scanner[] scanners = new Scanner[]{new Scanner(System.in), new Scanner(System.in)};
        String[] text = {"Привет", "Марсель", "А", "Помнишь?"};
        sort(users, new UserByNamesComparator());
//        sort(text);
        System.out.println(Arrays.toString(users));
//        System.out.println(Arrays.toString(text));
//        sort(scanners);
    }
}
