package com.akvelon.generics.comparing;

import java.util.Comparator;

/**
 * 06.04.2022
 * 02. Generics
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class ComparingUtil {
    public static <T extends Comparable<T>> void sort(T[] array) {
        sort0(array, null);
    }

    public static <T> void sort(T[] array, Comparator<T> comparator) {
        sort0(array, comparator);
    }

    private static <T> void sort0(T[] array, Comparator<T> comparator) {
        for (int i = array.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                int compareResult;
                if (comparator == null) {
                    compareResult = ((Comparable<T>)array[j]).compareTo(array[j+1]);
                } else {
                    compareResult = comparator.compare(array[j], array[j + 1]);
                }
                if (compareResult > 0) {
                    T temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }
}
