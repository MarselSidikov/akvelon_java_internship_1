package com.akvelon.generics.comparing;

import java.util.Comparator;

/**
 * 06.04.2022
 * 02. Generics
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UserByNamesComparator implements Comparator<User> {
    @Override
    public int compare(User o1, User o2) {
        int lastNameCompareResult = o1.getLastName().compareTo(o2.getLastName());
        if (lastNameCompareResult == 0) {
            return o1.getFirstName().compareTo(o2.getFirstName());
        }
        return lastNameCompareResult;
    }
}
