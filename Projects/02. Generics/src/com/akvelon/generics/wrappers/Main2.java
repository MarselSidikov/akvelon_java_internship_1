package com.akvelon.generics.wrappers;

/**
 * 06.04.2022
 * 02. Generics
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main2 {
    public static void main(String[] args) {
        // механизм кеширования, в Java есть кеши, все числа -128 до 128 имеют один и тот же оберточный объект
        Integer a = 10;
        Integer a1 = 10;
        Integer c = -10;
        Integer c1 = -10;
        Integer e = 120;
        Integer e1 = 120;
        Integer f = 129;
        Integer f1 = 129;

        System.out.println(a == a1);
        System.out.println(c == c1);
        System.out.println(e == e1);
        System.out.println(f == f1);

        //
    }
}
