package com.akvelon.generics.wrappers;

import com.akvelon.generics.example.GenericWrapper;

/**
 * 06.04.2022
 * 02. Generics
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        // boxing
        Integer i1 = new Integer(10);
        // unboxing
        int i2 = i1.intValue();

        // auto
        Integer i3 = 10;
        int i4 = i3;


    }
}
