package com.akvelon.generics.wildcards;

/**
 * 07.04.2022
 * 02. Generics
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Tiger extends Cat {
    @Override
    public void say() {
        System.out.println("Tiger");
    }
}
