package com.akvelon.generics.wildcards;

/**
 * 07.04.2022
 * 02. Generics
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Wolf extends Dog {
    @Override
    public void say() {
        System.out.println("Wolf");
    }
}
