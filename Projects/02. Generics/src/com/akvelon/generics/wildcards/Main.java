package com.akvelon.generics.wildcards;

import java.util.*;

/**
 * 07.04.2022
 * 02. Generics
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {

    public static void withRawType(List list) {
        for (int i = 0; i < list.size(); i++) {
            Object object = list.get(i);
        }
        list.add(new Scanner(System.in));
    }

    public static void withObjectsList(List<Object> objects) {
        for (int i = 0; i < objects.size(); i++) {
            Object object = objects.get(i);
        }
        objects.add(new Scanner(System.in));
    }

    public static void withAnimalsList(List<Animal> animals) {
        for (int i = 0; i < animals.size(); i++) {
            Animal animal = animals.get(i);
        }
        animals.add(new Cat());
    }

    public static void withWildcard(List<?> list) {
        for (int i = 0; i < list.size(); i++) {
            Object object = list.get(i);
        }
//        list.add(new Object());
//        list.add(new Scanner(System.in));
    }

    public static void withLowerBounds(List<? super Cat> list) {
        for (int i = 0; i < list.size(); i++) {
//            Tiger tiger = list.get(i);
//            Cat cat = list.get(i);
            Object object = list.get(i); // получить можно только Object
        }
//        list.add(new Dog());
//        list.add(new Animal());
        // можно класть Cat и потомков Cat
        list.add(new Cat());
        list.add(new Tiger());
    }

    public static void withUpperBounds(List<? extends Animal> list) {
        for (int i = 0; i < list.size(); i++) {
            Object object = list.get(i);
            Animal animal = list.get(i);
//            Scanner scanner = list.get(i);
//            Dog dog = list.get(i);
        }
//        list.add(new Object());
//        list.add(new Scanner(System.in));
//        list.add(new Animal());
//        list.add(new Dog());
//        list.add(new Cat());
    }

    public static void main(String[] args) {
        Creature creature = new Creature();
        Animal animal = new Animal();
        Cat cat = new Cat();
        Dog dog = new Dog();
        Wolf wolf = new Wolf();
        Tiger tiger = new Tiger();

        List<Creature> creatures = new ArrayList<>();
        List<Animal> animals = new ArrayList<>();
        List<Cat> cats = new ArrayList<>();
        List<Dog> dogs = new ArrayList<>();
        List<Wolf> wolves = new ArrayList<>();
        List<Tiger> tigers = new ArrayList<>();
        List<Scanner> scanners = new ArrayList<>();
        // если B extends A, то в List<A> можно класть объекты B и все остальные потомки
        creatures.add(creature);
        creatures.add(animal);
        creatures.add(cat);
        creatures.add(dog);
        creatures.add(wolf);
        creatures.add(tiger);

        Creature creatureFromList = creatures.get(0);
//        Animal animalFromList = creatures.get(1);

        // raw type
        List list = new ArrayList(); // почти как List<Object>
        list.add(creature);
        list.add(animal);
        list.add(cat);
        list.add(dog);
        list.add(wolf);
        list.add(tiger);
        list.add(new Scanner(System.in));


        Object fromList = list.get(0);

        scanners.add(new Scanner(System.in));

        withRawType(creatures);
        withRawType(animals);
        withRawType(scanners);

        ArrayList<Object> objects = new ArrayList<>();
//        withObjectsList(creatures);
        withObjectsList(objects);
//        withAnimalsList(tigers);
//        withAnimalsList(wolves);

        withWildcard(creatures);
        withWildcard(animals);
        withWildcard(scanners);

        withLowerBounds(objects);
        withLowerBounds(creatures);
        withLowerBounds(animals);
        withLowerBounds(cats);
//        withLowerBounds(tigers);
//        withLowerBounds(dogs);

//        withUpperBounds(scanners);
//        withUpperBounds(objects);
        withWildcard(animals);
        withWildcard(tigers);
        withWildcard(dogs);
        withWildcard(cats);
    }
}
