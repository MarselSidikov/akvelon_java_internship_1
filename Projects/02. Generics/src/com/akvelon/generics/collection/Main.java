package com.akvelon.generics.collection;

/**
 * 06.04.2022
 * 02. Generics
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        LinkedList<String> strings = new LinkedList<>();
        strings.add("Hello");
        strings.add("Bye");
//        strings.add(15);

        Iterator<String> stringIterator = strings.iterator();
        while (stringIterator.hasNext()) {
            String value = stringIterator.next();
            System.out.println(value);
        }
    }
}
