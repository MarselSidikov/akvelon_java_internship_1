package com.akvelon.generics.collection;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class LinkedList<V> implements List<V> {

    private Node<V> first;
    private Node<V> last;

    private int size;

    LinkedList() {
        this.first = null;
        this.size = 0;
    }

    @Override
    public void add(V element) {
        Node<V> newNode = new Node<>(element);

        if (size == 0) {
            this.first = newNode;
        } else {
            this.last.next = newNode;
        }
        this.last = newNode;
        size++;

    }

    @Override
    public V get(int index) {
        if (index >= 0 && index < size) {
            Node<V> current = first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }

            return current.value;
        } throw new IndexOutOfBoundsException();
    }

    @Override
    public Iterator<V> iterator() {
        return new LinkedListIterator();
    }

    private class LinkedListIterator implements Iterator<V> {
        private Node<V> current;

        public LinkedListIterator() {
            this.current = first;
        }

        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public V next() {
            V forReturnValue = current.value;
            current = current.next;
            return forReturnValue;
        }
    }

    private static class Node<T> {
        private final T value;

        private Node<T> next;

        Node(T value) {
            this.value = value;
        }
    }
}
