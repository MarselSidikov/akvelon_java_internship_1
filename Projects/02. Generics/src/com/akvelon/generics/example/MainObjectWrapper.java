package com.akvelon.generics.example;

/**
 * 06.04.2022
 * 02. Generics
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class MainObjectWrapper {

    public static void swap(Wrapper a, Wrapper b) {
        Object temp = a.getValue();
        a.setValue(b.getValue());
        b.setValue(temp);
    }

    public static void main(String[] args) {
        Wrapper x = new Wrapper(10);
        Wrapper y = new Wrapper("Hello");

        swap(x, y);

        System.out.println(x.getValue());
        System.out.println(y.getValue());
    }
}
