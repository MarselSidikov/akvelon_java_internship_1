package com.akvelon.object;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface Iterator {

    boolean hasNext();

    Object next();
}
