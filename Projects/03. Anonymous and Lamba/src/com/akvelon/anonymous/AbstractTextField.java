package com.akvelon.anonymous;

/**
 * 08.04.2022
 * 03. Anonymous and Lambda
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public abstract class AbstractTextField {
    protected String text;

    public abstract void beforeEdit();
    public abstract void afterEdit();

    public void setText(String text) {
        beforeEdit();
        this.text = text;
        afterEdit();
    }

    public String getText() {
        return text;
    }
}
