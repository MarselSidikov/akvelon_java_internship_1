package com.akvelon.anonymous;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        AbstractTextField textField = new AbstractTextField() {

            private final List<String> memory = new ArrayList<>();

            @Override
            public void beforeEdit() {
                if (this.text != null) {
                    this.memory.add(text);
                }
            }

            @Override
            public void afterEdit() {
                for (String fromMemory : memory) {
                    if (fromMemory.startsWith(text)) {
                        this.text = fromMemory;
                    }
                }
            }
        };

        textField.setText("JavaScript");
        textField.setText("Pascal");
        textField.setText("Java");
        System.out.println(textField.getText());

        System.out.println(textField.getClass().getName());

        String textForButton = "Hello!";

        Button myButton = new Button() {
            private String text = textForButton;

            @Override
            public void click() {
                System.out.println("Clicked on " + text);
            }

            @Override
            public void setText(String text) {
                this.text = text;
            }
        };

        myButton.click();
        myButton.setText("Bye");
        myButton.click();
    }
}
