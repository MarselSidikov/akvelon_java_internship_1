package com.akvelon.streams;

import com.akvelon.methods.Util;
import com.akvelon.standart.User;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * 11.04.2022
 * 03. Anonymous and Lamba
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {


    public static void main(String[] args) throws Exception {
        Stream<User> users = (new BufferedReader(new FileReader("input.txt")))
                .lines()
                .map(Util::map);

        users.forEach(System.out::println);

    }
}
