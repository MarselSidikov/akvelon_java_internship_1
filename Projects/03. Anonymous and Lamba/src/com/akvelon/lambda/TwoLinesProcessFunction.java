package com.akvelon.lambda;

/**
 * 08.04.2022
 * 03. Anonymous and Lamba
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface TwoLinesProcessFunction {
    String apply(String first, String second);
}
