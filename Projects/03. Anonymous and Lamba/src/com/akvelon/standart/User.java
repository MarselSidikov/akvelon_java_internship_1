package com.akvelon.standart;

import java.util.StringJoiner;

/**
 * 11.04.2022
 * 03. Anonymous and Lamba
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class User {
    private String name;
    private int age;
    private double weight;

    public User(String name, int age, double weight) {
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public double getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", User.class.getSimpleName() + "[", "]")
                .add("name='" + name + "'")
                .add("age=" + age)
                .add("weight=" + weight)
                .toString();
    }
}
