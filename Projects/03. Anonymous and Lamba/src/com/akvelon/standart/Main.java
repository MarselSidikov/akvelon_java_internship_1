package com.akvelon.standart;

import java.util.Scanner;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * 11.04.2022
 * 03. Anonymous and Lamba
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String userString = scanner.nextLine();

        Function<String, User> userMapper = string -> {
            String[] elements = string.split("\\|");
            String name = elements[0];
            int age = Integer.parseInt(elements[1]);
            double weight = Double.parseDouble(elements[2]);

            return new User(name, age, weight);
        };

        Predicate<User> userPredicate = user -> user.getAge() > 22;

        Consumer<User> userConsumer = user -> System.out.println(user);

        User user = userMapper.apply(userString);
        userConsumer.accept(user);
    }
}
