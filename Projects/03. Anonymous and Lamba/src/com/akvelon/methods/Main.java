package com.akvelon.methods;

import com.akvelon.standart.User;

import java.util.function.BiFunction;
import java.util.function.Function;

/**
 * 11.04.2022
 * 03. Anonymous and Lamba
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        User marsel = new User("Marsel", 28, 79);
//        Function<User, Integer> getAge = user -> user.getAge();
        Function<User, Integer> getAge = User::getAge;

//        Function<String, User> convert = string -> Util.map(string);
        Function<String, User> convert = Util::map;

//        BiFunction<User, String, User> addSurnameFunction = (user, surname) -> Util.addSurname(user, surname);
        BiFunction<User, String, User> addSurnameFunction = Util::addSurname;

        System.out.println(addSurnameFunction.apply(marsel, "Sidikov"));
    }
}
