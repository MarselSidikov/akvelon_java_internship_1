package com.akvelon.methods;

import com.akvelon.standart.User;

/**
 * 11.04.2022
 * 03. Anonymous and Lamba
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Util {
    public static User map(String string) {
        String[] elements = string.split("\\|");
        String name = elements[0];
        int age = Integer.parseInt(elements[1]);
        double weight = Double.parseDouble(elements[2]);

        return new User(name, age, weight);
    }

    public static User addSurname(User user, String surname) {
        return new User(user.getName() + " " + surname, user.getAge(), user.getWeight());
    }
}
