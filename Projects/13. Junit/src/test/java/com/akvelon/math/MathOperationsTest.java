package com.akvelon.math;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 28.04.2022
 * 13. Junit
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@DisplayName("MathOperations is working")
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
class MathOperationsTest {

    private MathOperations mathOperations;

    @BeforeEach
    public void setUp() {
        this.mathOperations = new MathOperations();
    }

    @DisplayName("isPrime() is working")
    @Nested
    class ForIsPrime {

        @ParameterizedTest(name = "return <false> on {0}")
        @ValueSource(ints = {121, 169})
        public void on_sql_numbers(int sqrNumber) {
            assertFalse(mathOperations.isPrime(sqrNumber));
        }

        @ParameterizedTest(name = "return <true> in {0}")
        @ValueSource(ints = {2, 3, 17, 31, 41, 13})
        public void on_primes_numbers(int primeNumber) {
            assertTrue(mathOperations.isPrime(primeNumber));
        }

        @ParameterizedTest(name = "throws exception on {0}")
        @ValueSource(ints = {-1, 0, 1})
        public void on_problem_numbers(int problemNumber) {
            assertThrows(IncorrectNumberException.class, () -> mathOperations.isPrime(problemNumber));
        }

        @ParameterizedTest(name = "return <false> on {0}")
        @ArgumentsSource(value = CompositeNumbersProvider.class)
        public void on_composite_numbers(int compositeNumber) {
            assertFalse(mathOperations.isPrime(compositeNumber));
        }

    }


}