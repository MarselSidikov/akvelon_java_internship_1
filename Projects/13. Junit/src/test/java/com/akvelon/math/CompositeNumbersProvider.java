package com.akvelon.math;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

/**
 * 28.04.2022
 * 13. Junit
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class CompositeNumbersProvider implements ArgumentsProvider {

    private final static int NUMBERS_COUNT = 5;
    private final static int HIGH = 100;
    private final static int LOW = 4;

    private final static Random random = new Random();

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        List<Arguments> arguments = new ArrayList<>();

        for (int i = 0; i < NUMBERS_COUNT; i++) {
            int first = LOW + random.nextInt(HIGH);
            int second = LOW + random.nextInt(HIGH);
            int composite = first * second;
            arguments.add(Arguments.of(composite));
        }
        return arguments.stream();
    }
}
