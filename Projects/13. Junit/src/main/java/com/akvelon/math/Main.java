package com.akvelon.math;

/**
 * 28.04.2022
 * 13. Junit
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        MathOperations operations = new MathOperations();

        System.out.println(operations.isPrime(7));
        System.out.println(operations.isPrime(121));
        System.out.println(operations.isPrime(31));
        System.out.println(operations.isPrime(56));
    }
}
