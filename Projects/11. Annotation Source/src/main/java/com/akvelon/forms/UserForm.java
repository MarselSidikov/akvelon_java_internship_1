package com.akvelon.forms;

import com.akvelon.html.HtmlForm;
import com.akvelon.html.HtmlInput;

import java.lang.annotation.RetentionPolicy;

/**
 * 26.04.2022
 * 11. Annotation Source
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@HtmlForm(method = "post", action = "/users")
public class UserForm {
//    @HtmlInput(type, name, placeholder, label)
    private String firstName;
    private String lastName;
    private String password;
    private String email;
}
