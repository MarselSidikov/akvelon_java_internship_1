package com.akvelon.example01;

import javafx.scene.control.PasswordField;

/**
 * 21.04.2022
 * 10. Reflection
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class PasswordValidator {

    public String validatorVersion = "Hello";

    private int minLength;

    public PasswordValidator(int minLength) {
        this.minLength = minLength;
    }
//
//    public PasswordValidator() {
//
//    }

    public PasswordValidator(String validatorVersion) {
        this.validatorVersion = validatorVersion;
    }

    public boolean validate(String password, boolean byLength) {
        return password.length() >= minLength;
    }

}
