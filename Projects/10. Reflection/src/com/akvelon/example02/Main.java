package com.akvelon.example02;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * 21.04.2022
 * 10. Reflection
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws ReflectiveOperationException {
        Class<?> aClass = Class.forName("com.akvelon.example01.PasswordValidator");

        Constructor<?> constructor = aClass.getDeclaredConstructor(int.class);

        Object object = constructor.newInstance(8);

        Method method = aClass.getMethod("validate", String.class, boolean.class);

        Field field = aClass.getDeclaredField("minLength");
        field.setAccessible(true);
        field.set(object, 15);

        Object result = method.invoke(object, "qwerty007",  true);

        System.out.println(result);
    }
}
