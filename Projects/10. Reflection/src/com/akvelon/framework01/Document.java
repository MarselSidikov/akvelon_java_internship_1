package com.akvelon.framework01;

import java.time.LocalDateTime;
import java.util.StringJoiner;

/**
 * 21.04.2022
 * 10. Reflection
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Document {
    private String title;
    private LocalDateTime createdAt;
    private boolean isProcessed;

    public Document(String title, LocalDateTime createdAt, boolean isProcessed) {
        this.title = title;
        this.createdAt = createdAt;
        this.isProcessed = isProcessed;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Document.class.getSimpleName() + "[", "]")
                .add("title='" + title + "'")
                .add("createdAt=" + createdAt)
                .add("isProcessed=" + isProcessed)
                .toString();
    }
}
