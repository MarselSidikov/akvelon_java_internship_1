package com.akvelon.framework02;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.LocalDateTime;

/**
 * 21.04.2022
 * 10. Reflection
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = ElementType.FIELD)
public @interface DefaultValue {
    String value() default "";


}
