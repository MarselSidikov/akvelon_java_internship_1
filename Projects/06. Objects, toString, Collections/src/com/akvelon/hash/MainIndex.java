package com.akvelon.hash;

public class MainIndex {

    public static void main(String[] args) {
        // Задача - любой хеш код уместить в нужный диапазон - [0, N - 1]
        int hash = "Seven".hashCode();

        System.out.println(hash);
        // N = 10, index -> [0, 9]
        int N = 16;
        int index = (N - 1) & hash;
        System.out.println(index);

        // hash = 79777773
        // N - 1 = 15
        // index = 13

        /*
                10011000001010011111110*00*
                                       1001
                                       ----
                00000000000000000000000*00*

                                       0000
                                       1000
                                       0001
                                       1001

               100110000010100111111100****
                                       1111
                                       ----
                                       0000 -> 1111
         */

    }
}
