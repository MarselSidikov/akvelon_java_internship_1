package com.akvelon.hash;

import com.akvelon.object.Human;

import java.util.HashMap;
import java.util.Map;

/**
 * 14.04.2022
 * 06. Objects, toString, Collections
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class MainHashInHashMap {

    static final int hash(Object key) {
        int h;
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

    public static void main(String[] args) {
        Human human0 = new Human("Марсель0", "Сидиков0", 28);
        Human human1 = new Human("Марсель1", "Сидиков1", 28);
        Human human2 = new Human("Марсель2", "Сидиков2", 28);
        Human human3 = new Human("Марсель3", "Сидиков3", 28);
        Human human4 = new Human("Марсель4", "Сидиков4", 28);
        Human human5 = new Human("Марсель5", "Сидиков5", 28);

        // карта высот для каждого человека
        Map<Double, Human> humanMap = new HashMap<>();
        humanMap.put(0.01, human0);
        humanMap.put(0.02, human1);
        humanMap.put(0.03, human2);
        humanMap.put(0.04, human3);
        humanMap.put(0.05, human4);
        humanMap.put(0.06, human5);

        System.out.printf("%5.2f - %13d - %32s - %32s \n", 0.01, Double.hashCode(0.01), Integer.toBinaryString(Double.hashCode(0.01)), Integer.toBinaryString(hash(0.01)));
        System.out.printf("%5.2f - %13d - %32s - %32s \n", 0.02, Double.hashCode(0.02), Integer.toBinaryString(Double.hashCode(0.02)), Integer.toBinaryString(hash(0.02)));
        System.out.printf("%5.2f - %13d - %32s - %32s \n", 0.03, Double.hashCode(0.03), Integer.toBinaryString(Double.hashCode(0.03)), Integer.toBinaryString(hash(0.03)));
        System.out.printf("%5.2f - %13d - %32s - %32s \n", 0.04, Double.hashCode(0.04), Integer.toBinaryString(Double.hashCode(0.04)), Integer.toBinaryString(hash(0.04)));
        System.out.printf("%5.2f - %13d - %32s - %32s \n", 0.05, Double.hashCode(0.05), Integer.toBinaryString(Double.hashCode(0.05)), Integer.toBinaryString(hash(0.05)));
        System.out.printf("%5.2f - %13d - %32s - %32s \n", 0.06, Double.hashCode(0.06), Integer.toBinaryString(Double.hashCode(0.06)), Integer.toBinaryString(hash(0.06)));
    }
}
