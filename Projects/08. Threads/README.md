# Многопоточное программирование

* Процесс - программа, имеет изолирование пространство, у каждого процесса свое адресное пространство и память.
* Поток - элемент процесса. Какое-либо вычисление, которое может выполняться параллельно (псевдопараллельно) с другими вычислениями.

## Многопоточность в Java

* В Java при старте существует один поток - main.
* В Java для потоков предусмотрен класс Thread.
* Поведение недетерминировано.
* Часто потоки используются, чтобы выполнять в отдельных потоках не сильно долгие задачи.
* Но создавать новый поток под каждую задачу - плохо.
  1) Создание потока - тяжелая операция для JVM
  2) Завершение потока - тяжелая операция для JVM
  3) Поддерживать работу сотни одновременно работающих потоков - тяжеловесная работа (не все потоки будут реально работать). 
## Методы класса Thread

* `Thread.currentThread();` - получение экземпляра текущего потока.
* `Thread.sleep(10000);` - усыпляет текущий поток
* `a.join()` - если в потоке `b` у потока `a` был вызван метод `join()`, то поток `b` будет ждать полного выполнения `a`. 

## Синхронизация потоков

* Семафор - объект, который контролирует (синхронизирует) работу нескольких потоков.
* Мьютекс - одноместный семафор, разрешает выполняться только одному потоку в момент времени.
* В Java любой объект может стать Mutex-ом.