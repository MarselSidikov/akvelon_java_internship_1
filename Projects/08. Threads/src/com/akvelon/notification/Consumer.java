package com.akvelon.notification;

/**
 * 19.04.2022
 * 08. Threads
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Consumer extends Thread {
    private final Product product;

    public Consumer(Product product) {
        super("Consumer");
        this.product = product;
    }

    @Override
    public void run() {
        while(true) {
            synchronized (product) {
                System.out.println("Consumer проверяет продукт");
                while (!product.isProduced()) {
                    try {
                        System.out.println("Consumer ушел в ожидание");
                        product.wait();
                    } catch (InterruptedException e) {
                        throw new IllegalStateException(e);
                    }
                }

                if (product.isProduced()) {
                    System.out.println("Consumer убедился, что можно использовать продукт");
                    product.consume();
                    System.out.println("Consumer использовал продукт");
                    // оповещает поток, который ждал на product
                    product.notify();
                    System.out.println("Consumer оповестил Producer");
                }
            }
        }
    }
}
