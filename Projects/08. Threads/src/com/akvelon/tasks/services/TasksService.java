package com.akvelon.tasks.services;

/**
 * 19.04.2022
 * 08. Threads
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface TasksService {
    void submit(Runnable task);
}
