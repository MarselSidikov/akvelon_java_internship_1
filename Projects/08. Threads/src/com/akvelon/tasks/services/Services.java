package com.akvelon.tasks.services;

/**
 * 19.04.2022
 * 08. Threads
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Services {
    public static TasksService threadPerTask() {
        return new TasksServiceThreadPerTaskImpl();
    }

    public static TasksService workerThread() {
        return new TaskServiceWorkerThreadImpl();
    }
}
