package com.akvelon.tasks.services;

import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.Callable;

/**
 * 19.04.2022
 * 08. Threads
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
class TaskServiceThreadPoolImpl implements TasksService {
    // очередь задач
    private final Deque<Runnable> tasks;
    // TODO: вам нужно, чтобы одновременно работало N-ое количество потоков и они брали на себя новые задачи
    private final WorkerThread workerThread;

    public TaskServiceThreadPoolImpl() {
        this.tasks = new LinkedList<>();
        this.workerThread = new WorkerThread();
        this.workerThread.start();
    }

    @Override
    public void submit(Runnable task) {
        synchronized (tasks) {
            tasks.add(task);
            tasks.notify();
        }
    }

    private class WorkerThread extends Thread {
        @Override
        public void run() {
            while (true) {
                synchronized (tasks) {
                    // пока задач нет
                    while (tasks.isEmpty()) {
                        try {
                            // уходим в ожидание
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalStateException(e);
                        }
                    }
                    Runnable task = tasks.poll();
                    task.run();
                }
            }
        }
    }
}
