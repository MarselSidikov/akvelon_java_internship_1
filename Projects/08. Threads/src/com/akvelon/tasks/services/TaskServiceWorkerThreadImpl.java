package com.akvelon.tasks.services;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 19.04.2022
 * 08. Threads
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
class TaskServiceWorkerThreadImpl implements TasksService {
    // очередь задач
    private final Deque<Runnable> tasks;

    private final WorkerThread workerThread;

    public TaskServiceWorkerThreadImpl() {
        this.tasks = new LinkedList<>();
        this.workerThread = new WorkerThread();
        this.workerThread.start();
    }

    @Override
    public void submit(Runnable task) {
        synchronized (tasks) {
            tasks.add(task);
            tasks.notify();
        }
    }

    private class WorkerThread extends Thread {
        @Override
        public void run() {
            while (true) {
                synchronized (tasks) {
                    // пока задач нет
                    while (tasks.isEmpty()) {
                        try {
                            // уходим в ожидание
                            tasks.wait();
                        } catch (InterruptedException e) {
                            throw new IllegalStateException(e);
                        }
                    }
                    Runnable task = tasks.poll();
                    task.run();
                }
            }
        }
    }
}
