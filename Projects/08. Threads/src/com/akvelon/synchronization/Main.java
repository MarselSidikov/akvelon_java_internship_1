package com.akvelon.synchronization;

/**
 * 19.04.2022
 * 08. Threads
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        CreditCard creditCard = new CreditCard(100);
        Human husband = new Human("муж", creditCard);
        Human wife = new Human("Жена", creditCard);
        husband.start();
        wife.start();
    }
}
