package com.akvelon.synchronization;

/**
 * 19.04.2022
 * 08. Threads
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Human extends Thread {

    private final String name;

    private static final int MAX_ORDERS_COUNT = 11;
    private static final int PRICE = 10;

    private final CreditCard creditCard;

    private static final Object MUTEX = new Object();

    public Human(String name, CreditCard creditCard) {
        this.creditCard = creditCard;
        this.name = name;
    }

    @Override
    public void run() {
        shopping();
    }

    public void shopping() {
        for (int i = 0; i < MAX_ORDERS_COUNT; i++) {
            synchronized (MUTEX) {
                System.out.println(name + " проверяет, есть ли деньги");
                if (creditCard.hasMoney(PRICE)) {
                    System.out.println(name + " убедился, что деньги есть, идем покупать");
                    creditCard.buy(PRICE);
                    System.out.println(name + " купил");
                } else {
                    System.out.println("Деньги кончились на " + name);
                }
            }

        }
    }

}
