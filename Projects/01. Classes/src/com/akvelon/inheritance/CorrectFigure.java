package com.akvelon.inheritance;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface CorrectFigure {
    int getSide();
}
