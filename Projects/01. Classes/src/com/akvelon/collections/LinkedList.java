package com.akvelon.collections;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
class LinkedList implements List {

    private Node first;
    private Node last;

    private int size;

    LinkedList() {
        this.first = null;
        this.size = 0;
    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);

        if (size == 0) {
            this.first = newNode;
        } else {
            this.last.next = newNode;
        }
        this.last = newNode;
        size++;

    }

    @Override
    public Iterator iterator() {
        // TODO: сделать итератор
        return null;
    }

    // Static Nested class
    private static class Node {
        private int value;

        private Node next;

        Node(int value) {
            this.value = value;
        }
    }
}
