package com.akvelon.collections;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
class ArrayList implements List {

    static final int INITIAL_SIZE = 10;

    private int[] elements;

    private int size;

    ArrayList() {
        this.elements = new int[INITIAL_SIZE];
        this.size = 0;
    }

    @Override
    public void add(int element) {
        grow(size + 1);
        this.elements[size++] = element;
    }

    @Override
    public Iterator iterator() {
        return new ArrayListIterator();
    }

    private void grow(int newSize) {
        // TODO: реализовать создание нового массива большей размерности
    }

    // Inner class
    private class ArrayListIterator implements Iterator {
        private int current = 0;

        @Override
        public boolean hasNext() {
            return current < size;
        }

        @Override
        public int next() {
            int forReturnValue = elements[current];
            current++;
            return forReturnValue;
        }
    }

}
