package com.akvelon.collections;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Lists {
    public static List arrayList() {
        return new ArrayList();
    }

    public static List linkedList() {
        return new LinkedList();
    }
}
