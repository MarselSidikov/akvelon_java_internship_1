package com.akvelon.abstracts;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Sequence sequence = new SequenceArraysSortImpl(new int[]{5, 10, 77, 81, -10, 5, 1, 3});
        System.out.println(sequence.exists(82)); // false
        System.out.println(sequence.exists(-11)); // false
        System.out.println(sequence.exists(10)); // true
        System.out.println(sequence.exists(81)); // true
        System.out.println(sequence.exists(-10)); // true
    }
}
