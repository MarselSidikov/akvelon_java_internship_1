package com.akvelon.abstracts;

/**
 * 05.04.2022
 * 01. Classes
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class SequenceBubbleSortImpl extends Sequence {

    public SequenceBubbleSortImpl(int[] elements) {
        super(elements);
    }

    @Override
    protected void sort() {
        for (int i = elements.length - 1; i >= 0; i--) {
            for (int j = 0; j < i; j++) {
                if (elements[j] > elements[j + 1]) {
                    int temp = elements[j];
                    elements[j] = elements[j + 1];
                    elements[j+1] = temp;
                }
            }
        }
    }
}
