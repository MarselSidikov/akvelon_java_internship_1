package com.akvelon;

import com.akvelon.jdbc.DriverManagerDataSource;
import com.akvelon.models.Pay;
import com.akvelon.repositories.PaysRepository;
import com.akvelon.repositories.PaysRepositoryJdbcImpl;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.time.LocalDate;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 04.05.2022
 * 14. Jdbc Example
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {

//        DriverManagerDataSource dataSource = new DriverManagerDataSource("C:\\Users\\Marsel\\Desktop\\Education\\akvelon_java_internship_1\\Projects\\14. Jdbc Example\\src\\main\\resources\\db.properties");

        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/akvelon_db");
        config.setUsername("postgres");
        config.setPassword("qwerty007");
        config.setMaximumPoolSize(20);

        HikariDataSource dataSource = new HikariDataSource(config);

        PaysRepository paysRepository = new PaysRepositoryJdbcImpl(dataSource);

        Pay pay = Pay.builder()
                .date(LocalDate.now())
                .description("номеР")
                .build();

        System.out.println(pay);
        paysRepository.save(pay);
        System.out.println(pay);

//        ExecutorService executorService = Executors.newFixedThreadPool(40);
//
//        for (int i = 0; i < 40; i++) {
//            final int finalI = i;
//            executorService.submit(() -> {
//                for (int j = 0; j < 1000; j++) {
//                    Pay pay = Pay.builder()
//                            .date(LocalDate.now())
//                            .description("номер " + finalI + " " + j)
//                            .build();
//
//
//                    try {
//                        paysRepository.save(pay);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//            });
    }
}

