package com.akvelon.repositories;

import java.util.List;
import java.util.Optional;

/**
 * 04.05.2022
 * 14. Jdbc Example
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface CrudRepository<T, ID> {
    void save(T entity);
    void update(T entity);

    Optional<T> findById(ID id);

    List<T> findAll();

    void delete(T entity);

    void deleteById(ID id);
}
