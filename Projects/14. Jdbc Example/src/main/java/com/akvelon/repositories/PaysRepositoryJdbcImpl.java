package com.akvelon.repositories;

import com.akvelon.models.Pay;

import javax.sql.DataSource;
import javax.xml.crypto.Data;
import java.sql.*;
import java.util.List;
import java.util.Optional;

/**
 * 04.05.2022
 * 14. Jdbc Example
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class PaysRepositoryJdbcImpl implements PaysRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into pay (date, description) values (?, ?);";

    private final DataSource dataSource;

    public PaysRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Pay entity) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            statement.setString(1, entity.getDate().toString());
            statement.setString(2, entity.getDescription());

            statement.executeUpdate();

            try (ResultSet generatedId = statement.getGeneratedKeys()) {
                if (generatedId.next()) {
                    entity.setId(generatedId.getLong("id"));
                } else throw new SQLException("Can't retrieve id");
            }
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Pay entity) {

    }

    @Override
    public Optional<Pay> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public List<Pay> findAll() {
        return null;
    }

    @Override
    public void delete(Pay entity) {

    }

    @Override
    public void deleteById(Long aLong) {

    }
}
