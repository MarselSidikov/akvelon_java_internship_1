# JDBC

* Java Database Connectivity - стандарт работы с базами данных в Java
* JDBC-драйвер - в общем виде это `jar-архив`, конкретнее - `класс`. Для каждой СУБД выпускается JDBC-драйвер,
который позволит вашей программе работать с конкретным типом СУБД.

## java.sql


* `Driver` - интерфейс, представляющий собой поведение JDBC-драйвера. Реализация этого интерфейса - `org.postgresql.Driver`

```
Connection connect(String url, java.util.Properties info);
```

* `DriverManager` - отвечает за поиск и подключение JDBC-драйверов.

```
private final static CopyOnWriteArrayList<DriverInfo> registeredDrivers = new CopyOnWriteArrayList<>();
```

* `Connection` - интерфейс, объекты которого представляют "сеанс" подключения к БД. Реализация - `org.postgresql.jdbc.PgConnection`

* `Statement` - интерфейс, объекты которого могут отправлять запросы в базы данных. Реализация - `org.postgresql.jdbc.PgStetament`

* `PreparedStatement` - интерфейс, объекты которого представляют подготовленные запросы. Реализация - `org.postgresql.jdbc.PgStetament`

* `ResultSet` - интерфейс, объекты которого представляют собой итераторы по результирующему множеству. Реализация - `org.postgresql.jdbc.PgResultSet`

* `DataSource` - интерфейс, объекты которого представляют собой источник данных (соединений).

## SQL Injection

* Исходный код

```
Scanner scanner = new Scanner(System.in);
String date = scanner.nextLine();
String description = scanner.nextLine();
String sql = "insert into pay(date, description) values ('" + date + "', '" + description + "')";
System.out.println(sql);
int affectedRows = statement.executeUpdate(sql);
System.out.println(affectedRows);
```

* Входные данные

```
2022-06-06
haha'); drop table account; insert into pay(date, description) values ('haha', 'haha
```

## Соединение с базой данных

### Вариант 1

* Оставить один `Connection` внутри репозитория. Слишком медленная работа, поскольку очень много запросов идут через 1 `Connection`.
* Сделать отдельный источник подключений, и для каждого запроса создавать новое подключение. Перегружается база данных, потому что одновременно может быть очень много запросов и это создает проблемы.
* Сделать ленивую инициализацию `Connection`, которая отрабатывает при отсутствии подключения или его закрытии (создается новое). 
При многопоточной работе могут быть проблемы с синхронизации и в итоге мы можем получить количество подключений, равное количеству потоков.

* Самое лучшее решение - `ConnectionPool`. Создается n-ое количество подключений и оно постоянно переиспользуется.