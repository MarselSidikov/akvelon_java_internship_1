package com.akvelon.homework02.services;

/**
 * 11.04.2022
 * 04. Architecture
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface AdminService {
    void addCourseToUser(Long courseId, Long userId);
}
