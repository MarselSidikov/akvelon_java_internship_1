package com.akvelon.homework02.repositories;

import com.akvelon.homework02.models.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * 11.04.2022
 * 04. Architecture
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersRepositoryArrayListImpl implements UsersRepository {
    private final List<User> users;

    public UsersRepositoryArrayListImpl() {
        this.users = new ArrayList<>();
    }

    @Override
    public void save(User entity) {

    }

    @Override
    public void update(User entity) {

    }

    @Override
    public void delete(User entity) {

    }

    @Override
    public Optional<User> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public List<User> findAll() {
        return null;
    }
}
