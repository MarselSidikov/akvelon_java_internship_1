package com.akvelon.homework02.repositories;

import com.akvelon.homework02.models.User;

/**
 * 11.04.2022
 * 04. Architecture
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface UsersRepository extends CrudRepository<User, Long> {
}
