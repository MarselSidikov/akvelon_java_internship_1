package com.akvelon.homework01.similiratity;

import java.util.List;

/**
 * 11.04.2022
 * 04. Architecture
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface Tokenizer {
    /**
     * Разбивает текст на слова
     * @param text входной текст
     * @return список слов
     */
    List<String> tokenize(String text);
}
