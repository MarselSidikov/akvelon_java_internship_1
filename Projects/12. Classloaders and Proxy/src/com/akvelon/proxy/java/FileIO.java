package com.akvelon.proxy.java;

import java.io.*;
import java.util.stream.Collectors;

/**
 * 27.04.2022
 * 12. Classloaders and Proxy
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class FileIO implements InputOutput {

    private final String fileName;

    public FileIO(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public String input() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))){
            return reader.lines().collect(Collectors.joining());
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void output(String text) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, true))){
            writer.write(text + " ");
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
