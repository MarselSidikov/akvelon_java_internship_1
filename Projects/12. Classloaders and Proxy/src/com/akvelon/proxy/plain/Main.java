package com.akvelon.proxy.plain;

/**
 * 27.04.2022
 * 12. Classloaders and Proxy
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {

    public static void sayHiFromHuman(Human human) {
        System.out.println("Этот человек говорит - ");
        human.hi();
    }

    public static void main(String[] args) {
        Human human = new Human("Марсель");

        HumanProxy proxy = new HumanProxy(human);
        proxy.setBefore(() -> System.out.println("Зайти в комнату"));
        proxy.setAfter(() -> System.out.println("Выйти из комнаты"));
        sayHiFromHuman(proxy);
    }
}
