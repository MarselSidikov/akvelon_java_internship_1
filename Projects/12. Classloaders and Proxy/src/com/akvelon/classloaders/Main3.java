package com.akvelon.classloaders;

/**
 * 27.04.2022
 * 12. Classloaders and Proxy
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main3 {
    public static void main(String[] args) {
        FolderClassLoader classLoader = new FolderClassLoader("C:\\Users\\Marsel\\Desktop\\Classes\\lib");
        try {
            Class<?> exampleClass = Class.forName("org.classes.Human", true, classLoader);
            System.out.println(exampleClass);
            System.out.println(exampleClass.getClassLoader());
            System.out.println(exampleClass.getDeclaredFields().length);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException(e);
        }

    }
}
