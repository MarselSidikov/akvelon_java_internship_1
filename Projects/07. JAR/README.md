# Работа с JAR

`javac -d target/ src/Program.java` - компиляция файла в папку target

`java com.akvelon.console.app.Program` - запуск приложения из папки target

`javac -cp lib/jcommander-1.82.jar -d target/ src/java/com/akvelon/console/*/*.java
` - компиляция нескольких java-файлов вместе в пакетами и сторонней библиотекой в папку `target` (делаем из корня проекта)

`cp -r src/resources target` - копирование ресурсов в целевую папку (делаем из корня проекта)

`jar xvf ../lib/jcommander-1.82.jar com` - распаковка стороннего архива в пакет com (делаем из папки target)

`jar -cvmf ../src/manifest.txt app.jar .` - сборка архива с MAINFEST-файлом (делаем из папки target)

`java -jar app.jar --numbers 1 --numbers 2 --numbers 3 --lower-case true` - запуск jar-архива (делаем из папки target)

