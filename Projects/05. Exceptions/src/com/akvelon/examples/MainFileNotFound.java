package com.akvelon.examples;

import java.io.FileInputStream;
import java.io.InputStream;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class MainFileNotFound {
    public static void main(String[] args) throws Exception {
        InputStream inputStream = new FileInputStream("input.txt");
    }
}
