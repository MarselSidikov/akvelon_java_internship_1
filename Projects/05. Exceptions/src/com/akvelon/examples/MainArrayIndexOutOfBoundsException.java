package com.akvelon.examples;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class MainArrayIndexOutOfBoundsException {
    public static void main(String[] args) {
        int[] array = new int[2];
        array[3] = 10;
    }
}
