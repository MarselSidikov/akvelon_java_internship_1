package com.akvelon.examples;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class MainOutOfMemoryError {
    public static void initialize() {
        String[] strings = new String[Integer.MAX_VALUE];
    }

    public static void main(String[] args) {
        initialize();
    }
}
