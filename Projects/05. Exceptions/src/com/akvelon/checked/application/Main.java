package com.akvelon.checked.application;

import com.akvelon.checked.application.repository.UsersRepository;
import com.akvelon.checked.application.repository.UsersRepositoryFileBasedImpl;
import com.akvelon.checked.application.service.UsersService;
import com.akvelon.checked.application.service.UsersServiceImpl;
import com.akvelon.checked.application.validators.EmailValidator;
import com.akvelon.checked.application.validators.SimpleEmailValidatorImpl;

import java.io.IOException;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) throws IOException {
        EmailValidator emailValidator = new SimpleEmailValidatorImpl();
        UsersRepository usersRepository = new UsersRepositoryFileBasedImpl("users.txt");
        UsersService usersService = new UsersServiceImpl(usersRepository, emailValidator);

        usersService.signIn("sidikov.marsel@gmail.com", "qwerty007");
//        usersService.signUp("simple@gmail.com", "asdf");

    }
}
