package com.akvelon.checked.application.repository;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Optional;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface UsersRepository {
    void save(User user);

    Optional<User> findByEmail(String email);
}
