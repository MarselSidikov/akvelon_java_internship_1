package com.akvelon.checked.application.validators.exceptions;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class EmailNotCorrectException extends RuntimeException {
}
