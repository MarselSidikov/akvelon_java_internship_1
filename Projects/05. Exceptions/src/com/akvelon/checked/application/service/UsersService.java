package com.akvelon.checked.application.service;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public interface UsersService {
    void signUp(String email, String password);

    void signIn(String email, String password);
}
