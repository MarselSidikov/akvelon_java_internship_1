package com.akvelon.checked.application.service;

import com.akvelon.checked.application.repository.User;
import com.akvelon.checked.application.repository.UsersRepository;
import com.akvelon.checked.application.service.exceptions.IncorrectPasswordException;
import com.akvelon.checked.application.service.exceptions.UserByEmailNotFoundException;
import com.akvelon.checked.application.validators.EmailValidator;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * 12.04.2022
 * 05. Exceptions
 *
 * @author Sidikov Marsel (Akvelon)
 * @version v1.0
 */
public class UsersServiceImpl implements UsersService {

    private final UsersRepository usersRepository;

    private final EmailValidator emailValidator;

    public UsersServiceImpl(UsersRepository usersRepository, EmailValidator emailValidator) {
        this.usersRepository = usersRepository;
        this.emailValidator = emailValidator;
    }

    @Override
    public void signUp(String email, String password) {
        emailValidator.validate(email);

        User user = new User(email, password);

        usersRepository.save(user);
    }

    @Override
    public void signIn(String email, String password) {
        User user = usersRepository.findByEmail(email).orElseThrow(UserByEmailNotFoundException::new);

        if (!user.getPassword().equals(password)) {
            throw new IncorrectPasswordException();
        }
    }
}
