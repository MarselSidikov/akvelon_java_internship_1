# Maven

* Решает вопросы сборки приложения (от исходного кода до jar-архива)
* Работа с зависимостями

## Базовая структура

* `src` - файлы исходного кода, ресурсы
  * `main` - основной исходный код + ресурсы
    * `java` - исходный код на Java
    * `resources` - файлы ресурсов
  * `test` - исходный код тестов и тестовые ресурсы
* `target` - целевая папка сборки
* `pom.xml` - файл проекта

## Как Maven работает с зависимостями?

* Сначала Maven ищет зависимость в уже скачанных библиотеках в локальном репозитории (папка `.m2`)
* Если зависимость не найдена - он скачивает и подключает к проекту.

## Центральные понятия Maven

- `Lifecycles` - `Build Lifecycle`
  - `Phases`
    - clean 
      - `maven-clean-plugin:2.5:clean`
    - compile
      - `maven-resources-plugin:2.6:resources`
      - `maven-compiler-plugin:3.1:compile`
    - package
      - `maven-resources-plugin:2.6:resources`
      - `maven-compiler-plugin:3.1:compile`
      - `maven-resources-plugin:2.6:testResources`
      - `maven-compiler-plugin:3.1:testCompile`
      - `maven-surefire-plugin:2.12.4:test`
      - `maven-jar-plugin:2.4:jar`
    - install - собирает jar-ник (`package`) - устанавливает jar в папку `.m2`, чтобы он был доступен из локального репозитория.
      - `maven-resources-plugin:2.6:resources`
      - `maven-compiler-plugin:3.1:compile`
      - `maven-resources-plugin:2.6:testResources`
      - `maven-compiler-plugin:3.1:testCompile`
      - `maven-surefire-plugin:2.12.4:test`
      - `maven-jar-plugin:2.4:jar`
      - `maven-install-plugin:2.4:install`
    
- `Goals`
- `Plugins`

`mvn clean package -DskipTests=true` - сборка из командной строки