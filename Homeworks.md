# Homeworks

## Requirements

1. создать репозиторий - `sidikov_homeworks_akvelon_internship_1` (указать Вашу фамилию)

2. в нем должна быть ветка `main` (с `.gitignore`), ветка `develop`

3. каждое задание - отдельное ветка вида `homework/номер_задания`

4. В репозитории каждое задание делается в отдельной папке вида HomeWorkНомер[01,02,03]

* Например, если было `Задание 25`, то ветка `homework/25`, в этой ветке папка `HomeWork25`

5. Каждое ДЗ - `merge-request` в `develop`, назначаете меня ассайнером - `sidikov.marsel@gmail.com`

6. Репозиторий скинуть в ЛС

##  Requirements 2.0

Для задач применим новый подход по проверке:

- `Education Center`
- `Homework05`
- `Homework06`
- `Homework07` 

Общая формулировка:
- Сначала `MR` оформляется из ветки задания в `develop` и проверяется одним из стажеров.
- После проверки, `MR` сливается проверяющим в `develop`
- Далее, автор делает `MR` на слияние `develop` в `main` (там уже может быть более одного ДЗ, это не проблема).

Приоритеты:
- На создание своей ДЗ и проверку чужой выделяется 2 дня (1 день выполнение, 1 день проверка, можете распоряжаться по своему)
- На третий день я ожидаю MR из `develop` в `main` и от проверяющего, и от автора

Замечания:
- Задача не может быть залита в develop, если она не удовлетворяет чек-листу
- Вы можете не исправлять все замечания, не относящиеся к чек-листу, но, если я буду согласен с этим замечанием, игнорирование этого считается ошибкой
- Все спорные случаи следует обсуждать со мной.

## Tasks

### Homework01

```
Реализовать приложение, которое считыват с консоли два текста и выдает значение их схожести.

В качестве алгоритма оценки схожести следует использовать косинусное сходство.

https://ru.wikipedia.org/wiki/%D0%92%D0%B5%D0%BA%D1%82%D0%BE%D1%80%D0%BD%D0%B0%D1%8F_%D0%BC%D0%BE%D0%B4%D0%B5%D0%BB%D1%8C#%D0%9A%D0%BE%D1%81%D0%B8%D0%BD%D1%83%D1%81%D0%BD%D0%BE%D0%B5_%D1%81%D1%85%D0%BE%D0%B4%D1%81%D1%82%D0%B2%D0%BE

Подсказка: формируете общий словарь, состоящий из слов обоих текстов. Далее, на основе каждого текста создаете числовой вектор встречаемости слова в каждом из текстов. Остается посчитать косинусную меру между векторами.
```

### Homework02

```
Реализовать сервис по заказу Такси.

Модели - Таксист, Пассажир
Сервис, который позволяет:
    - Пассажир может заказать такси в нужную точку
    - Таксист может принять или отменить заказ
    - Таксист может завершить поездку
    - Система запоминает все события, 
    которые связаны с заказами такси и поездках
    
Можно:
- Консольный интерфейс, либо написать Main в котором все это тестируется.
- Java Collection API

Нельзя:
- Использовать Java IO и т.д.
```

### Homework03

```
Сделать Homework03
    - Task 1
    - Task 2 
    - Task 3
    - screenshots
```

* [Task 1](https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/549/)
* [Task 2](https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/646/)
* [Task 3](https://leetcode.com/explore/interview/card/top-interview-questions-easy/92/array/674/)
* [Task 4](https://leetcode.com/problems/remove-nth-node-from-end-of-list/)
* [Task 5](https://leetcode.com/explore/interview/card/top-interview-questions-easy/93/linked-list/771/)
* [Task 6](https://leetcode.com/problems/remove-duplicates-from-sorted-list/)
* [Task 7](https://leetcode.com/problems/valid-parentheses/)
* [Task 8](https://leetcode.com/explore/interview/card/top-interview-questions-medium/109/backtracking/795/)
* [Task 9](https://leetcode.com/problems/diameter-of-binary-tree/)
* [Task 10](https://leetcode.com/explore/interview/card/top-interview-questions-easy/97/dynamic-programming/572/)
* [Task 11](https://leetcode.com/explore/interview/card/top-interview-questions-easy/98/design/562/)
* [Task 12](https://leetcode.com/problems/search-insert-position/)

### Homework Education Center


[Task](https://docs.google.com/document/d/1F5jrz-JfPAd09hquAH1jM3g3Mu-PNpjgo31BXstAvpU/edit?usp=sharing)


### Homework04

```
Подготовить файл с записями, имеющими следующую структуру:
[НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]

Используя Java Stream API, вывести:

1) Номера всех автомобилей, имеющих черный цвет или нулевой пробег.
2) Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.
3) Вывести цвет автомобиля с минимальной стоимостью.
4) Среднюю стоимость Camry
```

### Homework05

[Task](https://docs.google.com/document/d/1j5isioFKhC-pwXQeWqVRoH6eheSEMOvjqwc0LcDcLlw/edit?usp=sharing)

### Homework06

[Task](https://docs.google.com/document/d/1aqlY4mDKkOAQ2aGbQPDJptvqwu9uGN3wPZmwPCPmXg4/edit?usp=sharing)

## Homework07

[Task](https://docs.google.com/document/d/18kut2F6ZEyjKSVmQcOzuLVDMnHnd07OkLcqJmbxdAXU/edit?usp=sharing)

## Homework08

Два задания, в разных пакетах. Assign на меня.

1) Реализовать задачу:

```
Необходимо рассчитать сумму пассива параллельными потоками.
Каждый поток (а их всего threadsCount) берет определенную границу (границы должны быть равномерные) в массиве и считает сумму этой границы.

[   t1 ][   t2 ][   t3 ]

sum(t1) + sum(t2) + sum(t3)

При выводе в консоль ответы должны совпасть (классический и многопоточный)
```

2) Реализовать ThreadPool

## Homework09

- Реализовать библиотеку FilesDownloader, в которой есть класс с методом `download(String url)`.
- Реализовать приложение, которое использует FilesDownloader для скачивания в многопоточном режиме нескольких файлов. Вызов приложения:

```
java -jar multithreads-file-downloader.jar --files=URL1 --files=URL2 --fileURL3 --verbose
URL1 - downloaded in thread-0
URL2 - downloaded in thread-1
```

## Самостоятельно 

1) Централизованные системы контроля версий
2) `git rm -rf --cached .`
3) SOLID
4) Принцип работы IntegerCache и как изменить его границы
5) `compareTo` в классе `String`
6) Generics
7) Изучить [Документация](https://docs.oracle.com/javase/tutorial/java/javaOO/methodreferences.html), [Примеры](https://javatechonline.com/method-reference-java-8/)
8) Изучить [Stream API](https://habr.com/ru/company/luxoft/blog/270383/), [Better](https://stackify.com/streams-guide-java-8/)
9) Изучить Java IO

```
   - Serializable (serialVersionUID)
   - InputStream
   - OutputStream
   - FilterInputStream,
   - FilterOutputStream,
   - Reader
   - Writer
   - Buffered
```

10) Коллекции:

```
  - Queue/Deque
  - LinkedHashSet
  - SortedSet/TreeSet
  - SortedMap/NavigableMap

```
11) Дополнительный и обратный коды (хранение отрицательных чисел в компьютере)

12) Deadlock, Race condition, notifyAll() vs notify()
13) Maven archetypes